﻿using System;
using System.Reflection;
using System.Reflection.Emit;

class ReflectionExample
{
    public static void addMethod(TypeBuilder typeBuilder,
                                             string mthdName,
                                             Type[] mthdParams,
                                             Type returnType
                                             )
    {

        MethodBuilder methodBuilder = typeBuilder.DefineMethod(
                                             mthdName,
                                             MethodAttributes.Public |
                                             MethodAttributes.Static,
                                             returnType,
                                             mthdParams);

        ILGenerator ILout = methodBuilder.GetILGenerator();

        int numParams = mthdParams.Length;

        for (byte x = 0; x < numParams; x++)
        {
            ILout.Emit(OpCodes.Ldarg_S, x);
        }

        //contructing the div
        ILout.Emit(OpCodes.Div);

        //contructing the return stmt
        ILout.Emit(OpCodes.Ret);
    }

    public static void Main()
    {
        AppDomain domain = AppDomain.CurrentDomain;
        AssemblyName asmName = new AssemblyName();
        asmName.Name = "MyDynamicAsm";

        AssemblyBuilder asmBuilder = domain.DefineDynamicAssembly(
                                       asmName,
                                       AssemblyBuilderAccess.RunAndSave);

        ModuleBuilder module = asmBuilder.DefineDynamicModule("exampleDynamicAsm",
                                                                  "ExampleDynamicAsm.dll");

        TypeBuilder typeBuilder = module.DefineType("ExampleDynamicType",
                                                    TypeAttributes.Public);

        // User interface
        Console.WriteLine("--> Started building dinamic method <--");
        Console.WriteLine("Enter two numbers separated by space (numerator and denominator)");
        string userInputNums = Console.ReadLine();
        Console.Write("Choose a name for the new method:");
        string userMethodName = Console.ReadLine();

        // Process inputNums into an array 
        int index = 0;
        string[] inputNumsList = userInputNums.Split();

        Type[] methodParams = new Type[inputNumsList.Length];
        object[] inputValsList = new object[inputNumsList.Length];


        foreach (string inputNum in inputNumsList)
        {
            inputValsList[index] = (object)Convert.ToInt32(inputNum);
            methodParams[index] = typeof(int);
            index++;
        }


        addMethod(typeBuilder,
                             userMethodName,
                             methodParams,
                             typeof(int)
                             );

        Type myType = typeBuilder.CreateType();

        Console.WriteLine("--> Construction completed <-- \n\n");
        /*Console.WriteLine("The result of splitting the inputted values is: {0}",
                          myType.InvokeMember(userMethodName,
                          BindingFlags.InvokeMethod | BindingFlags.Public |
                          BindingFlags.Static,
                          null,
                          null,
                          inputValsList));
        Console.WriteLine("--> Method invoked succesfully --<");*/



        //Checking if the method is saved inside the asm library
        asmBuilder.Save("ExampleDynamicAsm.dll");

        MethodInfo myMthdInfo = myType.GetMethod(userMethodName);
        Console.WriteLine("Your Dynamic Method: {0};", myMthdInfo.ToString());


        Console.WriteLine("--> Loading generated dll library");
        var DLL = Assembly.LoadFile(@"\\Mac\Home\Documents\Visual Studio 2015\Projects\ReflectionExample\ReflectionExample\bin\Debug\ExampleDynamicAsm.dll");

        foreach (Type type in DLL.GetExportedTypes())
        {
            var c = Activator.CreateInstance(type);
            Console.WriteLine(c.ToString());
            Console.WriteLine("--> invoking {0} method from dll, result is:",userMethodName);
            Console.WriteLine(type.InvokeMember(userMethodName, BindingFlags.InvokeMethod, null, c, inputValsList));
        }

        Console.WriteLine("Press enter to exit");
        Console.ReadLine();
    }
}